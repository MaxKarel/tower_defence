#include <glm/gtc/random.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <ppgso/ppgso.h>
#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>

#include "nexus.h"
#include "monster.h"

// Static resources
std::unique_ptr<ppgso::Mesh> Hedron::mesh;
std::unique_ptr<ppgso::Texture> Hedron::texture;
std::unique_ptr<ppgso::Shader> Hedron::shader;

Hedron::Hedron(glm::vec3 pos, glm::vec3 np) {
    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("nexus.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("nexus.obj");

    nexus_pos = np;
    rotMomentum = glm::vec3(1.0f,1.0f, ppgso::PI - 1.5f);
    position = pos;
    scale = glm::vec3(0.3f);
}

bool Hedron::update(Scene &scene, float dt) {
    // Accumulate time
    time += dt;
    rotation += rotMomentum * dt;
    glm::vec3 pivot = glm::vec3(0.0f, -1.5f, 0.0f);
    modelMatrix =
        glm::translate(glm::mat4(1.0f), nexus_pos - pivot)
        * glm::translate(glm::mat4(1.0f), pivot)
        * glm::rotate(glm::mat4(1.0f), rotation.z, glm::vec3(0.0f,0.0f, 1.0f))
        * glm::translate(glm::mat4(1.0f),  -pivot)
        * glm::scale(glm::mat4(1.0f), scale);


    return true;
}

bool Hedron::update(Scene &scene, glm::mat4 &MM, float dt) {
    // Accumulate time
    time += dt;
    nexus_rot += rotMomentum * dt;
    glm::vec3 radius = glm::vec3(0.0f, 1.5f, 0.0f);

    modelMatrix = MM; // Rotate whole hedron plane
    modelMatrix *=
            glm::rotate(glm::mat4(1.0f), nexus_rot.z, glm::vec3(0.0f,0.0f, 1.0f)) // disperse hedrons on circle around point 0.0
            * glm::translate(glm::mat4(1.0f),  radius) // with radius
            * glm::scale(glm::mat4(1.0f), scale)
            * glm::translate(glm::mat4(1.0f), -nexus_pos); // Negate translation from nexus
    modelMatrix *= MM; // Apply same hedron rotation as nexus

    if(third_level) {
        small_hdr->update(scene, modelMatrix, dt);
    }
    return true;
}

void Hedron::render(Scene &scene) {
    if(third_level) {
        small_hdr->render(scene);
    }
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();
}

void Hedron::onClick(Scene &scene) {
    printf("hedron\n");
}

void Hedron::init_small_hedron(glm::vec3 pos) {
    auto tiny = std::make_unique<Hedron>(pos,pos);
    scale = glm::vec3(0.5f);
    tiny->scale = glm::vec3(4.0f);
    small_hdr = move(tiny);
    third_level = true;
}
#pragma once

#include "ppgso/ppgso.h"

#include "object.h"
#include "scene.h"

/*!
 * Example generator of objects
 * Constructs a new object during Update and adds it into the scene
 * Does not render anything
 */
class Spawner final : public Object {
private:
    // Static resources (Shared between instances)
    static std::unique_ptr<ppgso::Mesh> mesh;
    static std::unique_ptr<ppgso::Shader> shader;
    static std::unique_ptr<ppgso::Texture> texture;

    // Describes how many enemies will spawn
    int count = 6;
public:
    Spawner(Scene &scene, glm::vec3 pos, glm::vec3 rot);

    /*!
     * Generate new asteroids over time
     * @param scene Scene to update
     * @param dt Time delta
     * @return true to delete the object
     */
    bool update(Scene &scene, float dt) override;

    /*!
     * Render placeholder for generator
     * @param scene Scene to render in
     */
    void render(Scene &scene) override;

    float time = 0.0f;
};

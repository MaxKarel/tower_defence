#ifndef PPGSO_LEVEL_H
#define PPGSO_LEVEL_H

#include <ppgso/ppgso.h>

#include "object.h"

/*!
 * This object renders the scene background
 * It does not use the camera so it uses different 2D shader program
 * Background animation is achieved by passing an offset to the fragment shader that offsets texture mapping
 */
class Level final : public Object {
private:
    static const int board_size = 21;
    // Static resources (Shared between instances)
    static std::unique_ptr<ppgso::Mesh> mesh;
    static std::unique_ptr<ppgso::Shader> shader;
    static std::unique_ptr<ppgso::Texture> texture;
    bool game_board[board_size][board_size]{};
public:
    /*!
     * Create new Space background
     */
    Level(int level_id);

    /*!
     * Update space background
     * @param scene Scene to update
     * @param dt Time delta
     * @return true to delete the object
     */
    bool update(Scene &scene, float dt) override;

    /*!
     * Render space background
     * @param scene Scene to render in
     */
    void render(Scene &scene) override;

    void onClick(Scene &scene) override;

    void add_structure(Scene &scene, std::unique_ptr<Object> obj, glm::vec3 pos);
};

#endif //PPGSO_LEVEL_H

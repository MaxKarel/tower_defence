#include <glm/gtc/random.hpp>
#include <ppgso/ppgso.h>
#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>

#include "spawner.h"
#include "monster.h"

// Static resources
std::unique_ptr<ppgso::Mesh> Spawner::mesh;
std::unique_ptr<ppgso::Texture> Spawner::texture;
std::unique_ptr<ppgso::Shader> Spawner::shader;

Spawner::Spawner(Scene &scene, glm::vec3 pos, glm::vec3 rot) {
    position = pos;
    rotation = rot;
    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("spawner.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("spawner.obj");
}

bool Spawner::update(Scene &scene, float dt) {
    // Accumulate time
    time += dt;

    // Add object to scene when time reaches certain level
    if (time > 2 && count > 0) {
        auto obj = std::make_unique<Monster>();
        obj->position = position;
        obj->shadow = move(std::make_unique<Monster>());
        scene.objects.push_back(move(obj));
        count --;
        time = 0;
        if (count == 0) {
            scene.disable_spawner();
        }
    }

    generateModelMatrix();

    return true;
}

void Spawner::render(Scene &scene) {
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();
}

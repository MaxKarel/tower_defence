#include "level.h"
#include "scene.h"

#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>
#include <cmath>

// shared resources
std::unique_ptr<ppgso::Mesh> Level::mesh;
std::unique_ptr<ppgso::Shader> Level::shader;
std::unique_ptr<ppgso::Texture> Level::texture;

Level::Level(int level_id) {
    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);

    std::string obj_name = "level" + std::to_string(level_id) + ".obj";
    std::string texture_name = "level" + std::to_string(level_id) + ".bmp";
    mesh = std::make_unique<ppgso::Mesh>(obj_name);
    texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP(texture_name));

    rotation.z = ppgso::PI;
    scale.x = 10;
    scale.y = 10;

    for (auto &i : game_board) {
        for (auto &j : i) {
            j = false;
        }
    }
    if(level_id == 1) {
        for(int i = 0 ; i < board_size; i++) {
            game_board[8][i] = true;
            game_board[9][i] = true;
            game_board[10][i] = true;
            game_board[11][i] = true;
            game_board[12][i] = true;
        }
    } else {
        for(int i = 0 ; i < board_size; i++) {
            game_board[8][i] = true;
            game_board[9][i] = true;
            game_board[10][i] = true;
            game_board[11][i] = true;
            game_board[12][i] = true;
        }
    }
}

bool Level::update(Scene &scene, float dt) {
    generateModelMatrix();
    return true;
}

void Level::render(Scene &scene) {
    shader->use();

    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();
}

void Level::onClick(Scene &scene) {

}

void Level::add_structure(Scene &scene, std::unique_ptr<Object> obj, glm::vec3 pos) {
    int x = int(std::floor(pos.x));
    int y = int(std::floor(pos.y));
    if (x < -9 || x > 9 || y < -9 || y > 9) { return;}
    obj->position.x = (float)x;
    obj->position.y = (float)y;

    x+=10;
    y+=10;
    printf("%d %d %s\n",x,y,game_board[x][y]?"true":"false");
    if (game_board[x][y]) { return;}

    game_board[x][y] = true;
    scene.buildings.push_back(move(obj));
}
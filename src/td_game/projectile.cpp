#include <glm/gtc/random.hpp>
#include "scene.h"
#include "projectile.h"

#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>


// shared resources
std::unique_ptr<ppgso::Mesh> Projectile::mesh;
std::unique_ptr<ppgso::Shader> Projectile::shader;
std::unique_ptr<ppgso::Texture> Projectile::texture;

Projectile::Projectile(glm::vec3 pos, glm::vec3 tar) {
    position = pos;
    position.z -= 3.0f;
    target = tar;
    glm::vec3 direction = glm::normalize(target - position);
    speed = direction * 7.0f;
    rotation = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 u_n = glm::normalize(glm::vec3(direction.x, direction.y, 0.0f));
    glm::vec3 v_n = glm::vec3(0.0f,1.0f,0.0f);
    float angle = glm::acos(glm::dot(u_n, v_n));
    if (direction.x > 0) {
        angle *= -1.0f;
    }
    rotation.y = angle;
    // Set default speed
    rotMomentum = {0.0f, 0.0f, glm::linearRand(-ppgso::PI / 4.0f, ppgso::PI / 4.0f)};

    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("explosion.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("missile.obj");
}

bool Projectile::update(Scene &scene, float dt) {
    // Increase age
    age += dt;

    // Move the projectile
    position += speed * dt;

    // Die after 5s
    if (age > 5.0f)  {
        //printf("projectile died of old age\n");
        return false;
    }
    if (position.z > 0) {
        //printf("projectile died of low position\n");
        return false;
    }

    generateModelMatrix();
    return true;
}

void Projectile::render(Scene &scene) {
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();
}

void Projectile::destroy() {
    // This will destroy the projectile on Update
    age = 100.0f;
}

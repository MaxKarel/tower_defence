//
// Created by max on 11/24/19.
//

#ifndef PPGSO_RAIN_H
#define PPGSO_RAIN_H


#include "object.h"
#include "ppgso/ppgso.h"
#include "camera.h"

class Rain : public Object {
private:
    static std::unique_ptr<ppgso::Shader> shader;
    static std::unique_ptr<ppgso::Mesh> mesh;
    static std::unique_ptr<ppgso::Texture> texture;

    float age{0.0f};
    glm::vec3 speed;
public:
    Rain(glm::vec3 pos);
    bool update(Scene &scene, float dt) override;
    void render(Scene &scene) override;
    void destroy();
};


#endif //PPGSO_RAIN_H

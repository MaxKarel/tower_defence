#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <ppgso/ppgso.h>

/*!
 * Simple camera object that keeps track of viewMatrix and projectionMatrix
 * the projectionMatrix is by default constructed as perspective projection
 * the viewMatrix is generated from up, position and back vectors on update
 */
class Camera {
private:
    bool animateView = false;
    int animateSteps = 75;
    glm::vec3 target_up;
    glm::vec3 target_position;
    glm::vec3 target_back;
    glm::vec3 d_up;
    glm::vec3 d_position;
    glm::vec3 d_back;
    float fow, ratio, near, far;
public:
    glm::vec3 up{0, 1, 0};
    glm::vec3 position{0, 0, 0};
    glm::vec3 back{0, 0, -1};

    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;

    /*!
     * Create new Camera that will generate viewMatrix and projectionMatrix based on its position, up and back vectors
     * @param fow - Field of view in degrees
     * @param ratio - Viewport screen ratio (usually width/height of the render window)
     * @param near - Distance to the near frustum plane
     * @param far - Distance to the far frustum plane
     */
    Camera(float fow = 45.0f, float ratio = 1.0f, float near = 0.1f, float far = 10.0f);

    /*!
     * Update Camera viewMatrix based on up, position and back vectors
     */
    void update();
    void update_menu();

    /*!
     * Get direction vector in world coordinates through camera projection plane
     * @param u - camera projection plane horizontal coordinate [-1,1]
     * @param v - camera projection plane vertical coordinate [-1,1]
     * @return Normalized vector from camera position to position on the camera projection plane
     */
    glm::vec3 cast(double u, double v);

    /*!
     * Change camera view after pressing camera switch button
     * @param frontView - determines whether it is front view or side view
     */
    void setTargetView(bool frontView);

    /*!
     * Change field of view of camera. This will zoom in or out of the scene
     * @param df - rate of change of field of view
     */
    void zoom(float df);

    /*!
     * Convert screen space to world_space on game field
     * @param u - camera projection plane horizontal coordinate [-1,1]
     * @param v - camera projection plane vertical coordinate [-1,1]
     * @return point on plane z=0
     */
    glm::vec3 to_worldSpace(double u, double v);
};


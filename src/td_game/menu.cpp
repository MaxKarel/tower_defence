#include "menu.h"
#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>

std::unique_ptr<ppgso::Mesh> Menu::mesh;
std::unique_ptr<ppgso::Shader> Menu::shader;
std::unique_ptr<ppgso::Texture> Menu::start_screen;
std::unique_ptr<ppgso::Texture> Menu::victory_screen;
std::unique_ptr<ppgso::Texture> Menu::defeat_screen;

Menu::Menu() {
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!start_screen) start_screen = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("menu.bmp"));
    if (!victory_screen) victory_screen = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("victory.bmp"));
    if (!defeat_screen) defeat_screen = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("defeat.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("quad.obj");
    this->wind = glm::vec3(glm::linearRand(2.0f,4.0f),0.0f,0.0f);
    this->lightDirection = glm::vec3 (1.0f,1.0f,-1.0f);
    glm::vec3 pos = glm::vec3(-1.0f,-1.0f,-1.0f);
    glm::vec3 rot = glm::vec3(1.0f);
    auto lvl_1 = std::make_unique<Nexus>(*this,pos,rot);
    lvl_1->decorate_hedrons();
    objects.push_back(move(lvl_1));

}

void Menu::update(float dt, float time) {
    camera->update_menu();

    rain_age += dt;
    if (rain_age > 0.05f) {
        rain_age = 0.0f;
        auto r_pos = glm::vec3(glm::linearRand(-5.0f,5.0f), 4.0f,0.0f);
        auto raindrop = std::make_unique<Rain>(r_pos);
        objects.push_back(move(raindrop));
    }
    // Use iterator to update all objects so we can remove while iterating
    auto o = std::begin(objects);

    while (o != std::end(objects)) {
        // Update and remove from list if needed
        auto obj = o->get();
        if (!obj->update(*this, dt))
            o = objects.erase(o); // NOTE: no need to call destructors as we store shared pointers in the scene
        else
            ++o;
    }

    modelMatrix =
            glm::translate(glm::mat4(1.0f), position)
            * glm::orientate4(rotation)
            * glm::scale(glm::mat4(1.0f), scale);
}

void Menu::render() {
    // Disable writing to the depth buffer so we render a "background"
    //  glEnable(GL_DEPTH_TEST);
    // NOTE: this object does not use camera, just renders the entire quad as is
    shader->use();
    // Render mesh, not using any projections, we just render in 2D
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("ViewMatrix", camera->viewMatrix);
    shader->setUniform("ProjectionMatrix", camera->projectionMatrix);
    shader->setUniform("LightDirection", lightDirection);

    if (start_menu) {
        shader->setUniform("Texture", *start_screen);
    } else if (victory) {
        shader->setUniform("Texture", *victory_screen);
    } else {
        shader->setUniform("Texture", *defeat_screen);
    }
    mesh->render();

    for (auto &obj : objects) {
        obj->render(*this);
    }
}

void Menu::render(Scene &scene) {
    printf("pass");
}

bool Menu::update(Scene &scene, float dt) {
    printf("pass");
    return true;
}
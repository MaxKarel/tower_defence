//
// Created by max on 11/24/19.
//

#include "rain.h"
#include "scene.h"

#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>


// shared resources
std::unique_ptr<ppgso::Mesh> Rain::mesh;
std::unique_ptr<ppgso::Shader> Rain::shader;
std::unique_ptr<ppgso::Texture> Rain::texture;

Rain::Rain(glm::vec3 pos) {
    position = pos;
    scale = glm::vec3(glm::linearRand(0.01f, 0.03f));
    speed = glm::vec3(0.0f);
    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("explosion.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("monster1.obj");
}

bool Rain::update(Scene &scene, float dt) {
    age += dt;
    if (age > 5.0f) {
        return false;
    }

    position += speed * dt;
    if (position.y < -10.0f) {
        return false;
    }

    speed += scene.wind * dt;
    speed += scene.gravity * dt;

    generateModelMatrix();
    return true;
}

void Rain::render(Scene &scene) {
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera

    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();
}
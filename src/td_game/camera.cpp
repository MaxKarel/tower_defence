#include <glm/glm.hpp>

#include "camera.h"


Camera::Camera(float fow, float ratio, float near, float far) {
    float fowInRad = (ppgso::PI / 180.0f) * fow;
    this->fow = fow;
    this->ratio = ratio;
    this->near = near;
    this->far = far;
    projectionMatrix = glm::perspective(fowInRad, ratio, near, far);
}

void Camera::update() {
    if (animateView) {
        position += d_position;
        back += d_back;
        up += d_up;
        glm::vec3 t = position - target_position;
        float distance = glm::length(t);
        if (distance < 0.01) {
            animateView = false;
            position = target_position;
            back = target_back;
            up = target_up;
        }
    }

    glm::vec3 center = position - back;

    viewMatrix = lookAt(position, center, up);
}

void Camera::update_menu() {
    glm::vec3 p{0,0,-6.66f}; //0,-10,10
    glm::vec3 t{0,0,0};
    glm::vec3 u{0,1,0};
    viewMatrix = lookAt(p, t, u);
}

glm::vec3 Camera::cast(double u, double v) {
    // Create point in Screen coordinates
    glm::vec4 screenPosition{u, v, 0.0f, 1.0f};

    // Use inverse matrices to get the point in world coordinates
    auto invProjection = glm::inverse(projectionMatrix);
    auto invView = glm::inverse(viewMatrix);

    // Compute position on the camera plane
    auto planePosition = invView * invProjection * screenPosition;
    planePosition /= planePosition.w;

    // Create direction vector
    auto direction = glm::normalize(planePosition - glm::vec4{position, 1.0f});
    return glm::vec3{direction};
}

glm::vec3 Camera::to_worldSpace(double u, double v) {
    glm::vec4 screenPosition{u, v, 0.0f, 1.0f};
    auto invProjection = glm::inverse(projectionMatrix);
    auto invView = glm::inverse(viewMatrix);
    auto planePosition = invView * invProjection * screenPosition;
    planePosition /= planePosition.w;
    auto direction = glm::normalize(planePosition - glm::vec4{position, 1.0f});
    glm::vec3 ray_world = glm::vec3(direction.x, direction.y, direction.z);

    glm::vec3 norm = glm::vec3(0.0,0.0,1.0);
    float d = glm::dot(ray_world, norm);
    if ( d > 1e-6) {
        float t = glm::dot(-position, norm) / d;
        glm::vec3 result = ray_world * t;
        result += position;
        result.z = 0;
        return result;
    }
    return glm::vec3(1.0f);
}

void Camera::setTargetView(bool frontView) {
    if (frontView) {
        target_position = glm::vec3(0, -15, -15);
        target_back = glm::vec3(0, -1, -1);
        target_up = glm::vec3(0, 1, 0);
    } else {
        target_position = glm::vec3(-15, 0, -15);
        target_back = glm::vec3(-1, 0, -1);
        target_up = glm::vec3(1, 0, 0);
    }
    d_position = (target_position - position);
    d_position /= animateSteps;
    d_back = (target_back - back);
    d_back /= animateSteps;
    d_up = (target_up - up);
    d_up /= animateSteps;
    animateView = true;
}

void Camera::zoom(float df) {
    fow += df;
    float fowInRad = (ppgso::PI / 180.0f) * fow;
    std::cout << fow << std::endl;
    projectionMatrix = glm::perspective(fowInRad, ratio, near, far);
}

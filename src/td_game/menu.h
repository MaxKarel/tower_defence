#ifndef _PPGSO_MENU_H
#define _PPGSO_MENU_H

#include <memory>
#include <map>
#include <list>

#include <glm/gtx/euler_angles.hpp>
#include "object.h"
#include "camera.h"
#include "scene.h"
#include "rain.h"
#include "nexus.h"
#include "hedron.h"

/*
 * Scene is an object that will aggregate all scene related data
 * Objects are stored in a list of objects
 * Keyboard and Mouse states are stored in a map and struct
 */
class Menu : public Scene{
public:
    static std::unique_ptr<ppgso::Mesh> mesh;
    static std::unique_ptr<ppgso::Shader> shader;
    static std::unique_ptr<ppgso::Texture> start_screen;
    static std::unique_ptr<ppgso::Texture> victory_screen;
    static std::unique_ptr<ppgso::Texture> defeat_screen;
    bool start_menu = true;
    bool victory = true;
    glm::vec3 position{0.0f,0.0f,0.0f};
    glm::vec3 rotation{ppgso::PI,ppgso::PI,0.0f};
    glm::vec3 scale{2.5f};
    glm::mat4 modelMatrix{1.0f};
    float rain_age {0.0f};

    Menu();
    /*!
     * Update all objects in the scene
     * @param time
     */
    void update(float dt, float time);
    bool update(Scene &scene, float dt);

    /*!
     * Render all objects in the scene
     */
    void render();
    void render(Scene &scene);

    // Lights, in this case using only simple directional diffuse lighting
    glm::vec3 lightDirection{-1.0f, -1.0f, -1.0f};

};

#endif // _PPGSO_MENU_H

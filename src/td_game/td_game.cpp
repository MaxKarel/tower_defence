#include <iostream>
#include <map>
#include <list>

#include <ppgso/ppgso.h>

#include "camera.h"
#include "scene.h"
#include "spawner.h"
#include "nexus.h"
#include "level.h"
#include "menu.h"
#include "tower.h"

const unsigned int SIZE = 1000;

/*!
 * Custom windows for our simple game
 */
class SceneWindow : public ppgso::Window {
private:
    Scene scene;
    Menu menu;
    bool animate = true;
    bool menu_open = true;
    bool front_view = true;
    int level_id = 1;
    int level_count = 2;

    void initMenu() {
        // Create a camera
        auto camera = std::make_unique<Camera>(40.0f, 1.0f, 0.1f, 60.0f);
        menu.camera = move(camera);
    }

    /*!
     * Reset and initialize the game scene
     * Creating unique smart pointers to objects that are stored in the scene object list
     */
    void initScene() {
        scene.objects.clear();
        scene.buildings.clear();
        scene.end_game = false;
        scene.victory = false;
        scene.spawner_count = 1;

        // Create a camera
        auto camera = std::make_unique<Camera>(60.0f, 1.0f, 0.1f, 100.0f);
        camera->position = glm::vec3(0, -15, -15);
        camera->back = glm::vec3(0, -1, -1);
        camera->up = glm::vec3(0, 1, 0);
        scene.camera = move(camera);

        // Add space background
        scene.level = move(std::make_unique<Level>(level_id));
        // Add Nexus to scene
        scene.nexus = move(std::make_unique<Nexus>(scene, glm::vec3(0, -10, -0.7), glm::vec3(-ppgso::PI / 2, 0, 0)));

        // Add Spawner to scene
        auto spawner = std::make_unique<Spawner>(scene, glm::vec3(0, 10, -0.7), glm::vec3(-ppgso::PI / 2, 0, 0));
        scene.buildings.push_back(move(spawner));
    }


public:
    /*!
     * Construct custom game window
     */
    SceneWindow() : Window{"tower defence", SIZE, SIZE} {
        //hideCursor();
        glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);

        // Initialize OpenGL state
        // Enable Z-buffer
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        // Enable polygon culling
        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_BACK);

        initMenu();
    }

    /*!
     * Handles pressed key when the window is focused
     * @param key Key code of the key being pressed/released
     * @param scanCode Scan code of the key being pressed/released
     * @param action Action indicating the key state change
     * @param mods Additional modifiers to consider
     */
    void onKey(int key, int scanCode, int action, int mods) override {
        scene.keyboard[key] = action;


        // Pause
        if (key == GLFW_KEY_P && action == GLFW_PRESS) {
            animate = !animate;
        }

        if (key == GLFW_KEY_C && action == GLFW_PRESS) {
            if (menu_open)
                return;
            front_view = !front_view;
            scene.camera->setTargetView(front_view);
        }
        // Strat game from menu
        if (key == GLFW_KEY_S && action == GLFW_PRESS && menu_open) {
            menu_open = !menu_open;
            menu.start_menu = false;
            level_id = 1;
            initScene();
        }

        if (key == GLFW_KEY_PAGE_UP && action == GLFW_REPEAT) {
            scene.camera->zoom(0.5);
        }
        if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_REPEAT) {
            scene.camera->zoom(-0.5);
        }
    }

    /*!
     * Handle cursor position changes
     * @param cursorX Mouse horizontal position in window coordinates
     * @param cursorY Mouse vertical position in window coordinates
     */
    void onCursorPos(double cursorX, double cursorY) override {
        scene.cursor.x = cursorX;
        scene.cursor.y = cursorY;
    }

    /*!
     * Handle cursor buttons
     * @param button Mouse button being manipulated
     * @param action Mouse bu
     * @param mods
     */
    void onMouseButton(int button, int action, int mods) override {
        if (menu_open) {
            return;
        }
        if (button == GLFW_MOUSE_BUTTON_LEFT) {
            scene.cursor.left = action == GLFW_PRESS;

            if (scene.cursor.left) {
                // Convert pixel coordinates to Screen coordinates
                double u = (scene.cursor.x / width - 0.5f) * 2.0f;
                double v = -(scene.cursor.y / height - 0.5f) * 2.0f;

                // Get mouse pick vector in world coordinates
                auto position = scene.camera->to_worldSpace(u, v);
                // Add tower
                auto tower = std::make_unique<Tower>(scene, position);
                scene.level->add_structure(scene, move(tower), position);
            }
        }
        if (button == GLFW_MOUSE_BUTTON_RIGHT) {
            scene.cursor.right = action == GLFW_PRESS;
        }
    }
    void render_menu(float dt, float time) {
        menu.update(dt, time);
        menu.render();
    }
    void update_objects(float dt, float time) {
        // Update and render all objects
        scene.update(dt, time);
        scene.render();
    }
    /*!
     * Window update implementation that will be called automatically from pollEvents
     */
    void onIdle() override {
        // Track time
        static auto time = (float) glfwGetTime();
        // Compute time delta
        float dt = animate ? (float) glfwGetTime() - time : 0;
        time = (float) glfwGetTime();
        // Set gray background
        glClearColor(.5f, .5f, .5f, 0);
        // Clear depth and color buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (menu_open) {
            render_menu(dt, time);
            return;
        }
        if (!scene.end_game) {
            update_objects(dt, time);
        } else if (scene.victory) {
            if (level_count == level_id) {
                menu.victory = true;
                menu_open = true;
            } else {
                level_id++;
                initScene();
            }
        } else {
            menu.victory = false;
            menu_open = true;
        }
    }
};

int main() {
    // Initialize our window
    SceneWindow window;

    // Main execution loop
    while (window.pollEvents()) {}

    return EXIT_SUCCESS;
}

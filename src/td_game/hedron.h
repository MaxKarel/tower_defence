#pragma once

#include "ppgso/ppgso.h"

#include "object.h"

/*!
 * Example generator of objects
 * Constructs a new object during Update and adds it into the scene
 * Does not render anything
 */
class Hedron final : public Object {
private:
    // Static resources (Shared between instances)
    static std::unique_ptr<ppgso::Mesh> mesh;
    static std::unique_ptr<ppgso::Shader> shader;
    static std::unique_ptr<ppgso::Texture> texture;
    glm::vec3 nexus_pos;
    glm::vec3 rotMomentum;
    int hp = 1;

public:
    glm::vec3 nexus_rot;
    bool third_level = false;
    std::unique_ptr<Hedron> small_hdr;
    Hedron(glm::vec3 pos, glm::vec3 np);

    /*!
     * Generate new asteroids over time
     * @param scene Scene to update
     * @param dt Time delta
     * @return true to delete the object
     */
    bool update(Scene &scene, float dt) override;
    bool update(Scene &scene, glm::mat4 &MM, float dt);

    /*!
     * Render placeholder for generator
     * @param scene Scene to render in
     */
    void render(Scene &scene) override;

    float time = 0.0f;

    void onClick(Scene &scene) override;

    void init_small_hedron(glm::vec3 pos);
};

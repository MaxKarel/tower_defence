#include <glm/gtc/random.hpp>
#include <ppgso/ppgso.h>
#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>

#include "nexus.h"
#include "monster.h"

// Static resources
std::unique_ptr<ppgso::Mesh> Nexus::mesh;
std::unique_ptr<ppgso::Texture> Nexus::texture;
std::unique_ptr<ppgso::Shader> Nexus::shader;

Nexus::Nexus(Scene &scene, glm::vec3 pos, glm::vec3 rot) {
    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("nexus.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("nexus.obj");
    scale = glm::vec3(1.0f);
    rotMomentum = glm::ballRand(ppgso::PI);
    position = pos;
    alive = true;
    int hp = 5;
    for(int i = 0 ; i < hp ; i++) {
        auto h = std::make_unique<Hedron>(position,position);
        h->nexus_rot = glm::vec3(0.0f, 0.0f, 2.0f * (float)i * ppgso::PI / (float)hp);
        hedrons.push_back(move(h));
    }
}

bool Nexus::update(Scene &scene, float dt) {
    // Accumulate time
    time += dt;
    rotation += rotMomentum * dt;

    generateModelMatrix();

    for (auto &obj : hedrons)
        obj->update(scene, this->modelMatrix, dt);

    return alive;
}

void Nexus::render(Scene &scene) {
    for (auto &obj : hedrons)
        obj->render(scene);
    //return;
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();

}

void Nexus::onClick(Scene &scene) {
    printf("nexus\n");
}

bool Nexus::destroy() {
    hp--;
    if (hp < 1) {
        alive = false;
        printf("You lost the game\n");
    } else {
        hedrons.pop_back();
    }

    return alive;
}

void Nexus::decorate_hedrons() {
    scale = glm::vec3(0.5f);
    auto o = std::begin(hedrons);

    while (o != std::end(hedrons)) {
        // Update and remove from list if needed
        auto obj = o->get();
        obj->init_small_hedron(obj->position);
        ++o;
    }
}
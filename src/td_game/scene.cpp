#include "scene.h"

void Scene::update(float dt, float time) {
    camera->update();
    lightDirection.x = 15.0f * cos(time);

    level->update(*this, dt);

    if(! nexus->update(*this, dt)) {
        // nexus is not alive
        end_game = false;
        victory = false;
    }

    auto i = std::begin(buildings);

    while (i != std::end(buildings)) {
        // Update and remove from list if needed
        auto obj = i->get();
        if (!obj->update(*this, dt)) {
            i = buildings.erase(i);
        } // NOTE: no need to call destructors as we store shared pointers in the scene
        else {
            ++i;
        }
    }
    // Use iterator to update all objects so we can remove while iterating
    auto o = std::begin(objects);

    while (o != std::end(objects)) {
        // Update and remove from list if needed
        auto obj = o->get();
        if (!obj->update(*this, dt))
            o = objects.erase(o); // NOTE: no need to call destructors as we store shared pointers in the scene
        else
            ++o;
    }

    if (objects.empty() && spawner_count < 1) {
        end_game = true;
        victory = true;
    } else if(!nexus->alive) {
        end_game = true;
        victory = false;
    }
}

void Scene::render() {
    // Simply render all objects
    level->render(*this);
    nexus->render(*this);
    for (auto &obj : buildings)
        obj->render(*this);
    for (auto &obj : objects) {
        obj->render(*this);
    }
}

std::vector<Object *> Scene::intersect(const glm::vec3 &position, const glm::vec3 &direction) {
    std::vector<Object *> intersected = {};
    for (auto &object : objects) {
        // Collision with sphere of size object->scale.x
        auto oc = position - object->position;
        auto radius = object->scale.x;
        auto a = glm::dot(direction, direction);
        auto b = glm::dot(oc, direction);
        auto c = glm::dot(oc, oc) - radius * radius;
        auto dis = b * b - a * c;

        if (dis > 0) {
            auto e = sqrt(dis);
            auto t = (-b - e) / a;

            if (t > 0) {
                intersected.push_back(object.get());
                continue;
            }

            t = (-b + e) / a;

            if (t > 0) {
                intersected.push_back(object.get());
                continue;
            }
        }
    }

    return intersected;
}

void Scene::disable_spawner() {
    spawner_count--;
}
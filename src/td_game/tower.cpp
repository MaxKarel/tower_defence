#include <glm/gtc/random.hpp>
#include <ppgso/ppgso.h>
#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>

#include "tower.h"
#include "monster.h"

// Static resources
std::unique_ptr<ppgso::Mesh> Tower::mesh;
std::unique_ptr<ppgso::Texture> Tower::texture;
std::unique_ptr<ppgso::Shader> Tower::shader;

Tower::Tower(Scene &scene, glm::vec3 pos) {
    printf("  %f, %f, %f \n", pos.x, pos.y, pos.z);
    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("tower.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("tower.obj");

    position = pos;
    rotation = glm::vec3(-ppgso::PI / 2, 0, 0);
    scale = glm::vec3(0.7, 0.7, 0.7);
}

bool Tower::update(Scene &scene, float dt) {
    // Accumulate time
    fireDelay += dt;

    generateModelMatrix();

    for (auto &obj : scene.objects) {
        // Ignore self in scene
        if (obj.get() == this) continue;

        // Check only on monster objects
        auto monster = dynamic_cast<Monster*>(obj.get()); // dynamic_pointer_cast<Asteroid>(obj);
        if (!monster) continue;

        // Compare distance to approximate size of the asteroid estimated from scale.
        if (fireDelay > fireRate && distance(position, obj->position) < 5.00f) {
            // Fire arrow
            fireDelay = 0;
            auto projectile = std::make_unique<Projectile>(position, obj->position);
            scene.objects.push_back(move(projectile));
            break;
        }
    }

    return true;
}

void Tower::render(Scene &scene) {
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();
}

#include <glm/gtc/random.hpp>
#include "monster.h"
#include "projectile.h"
#include "explosion.h"
#include "nexus.h"

#include <shaders/diffuse_vert_glsl.h>
#include <shaders/diffuse_frag_glsl.h>


// Static resources
std::unique_ptr<ppgso::Mesh> Monster::mesh;
std::unique_ptr<ppgso::Texture> Monster::texture;
std::unique_ptr<ppgso::Texture> Monster::shadow_texture;
std::unique_ptr<ppgso::Shader> Monster::shader;

Monster::Monster() {
    // Set random scale speed and rotation
    scale *= 0.3f;
    speed = glm::vec3(0, -1.0f, 0);
    rotation = glm::vec3(0, 0, 0);
    rotMomentum = glm::vec3(0, 0, 1);

    // Initialize static resources if needed
    if (!shader) shader = std::make_unique<ppgso::Shader>(diffuse_vert_glsl, diffuse_frag_glsl);
    if (!texture) texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("monster1.bmp"));
    if (!shadow_texture) shadow_texture = std::make_unique<ppgso::Texture>(ppgso::image::loadBMP("black.bmp"));
    if (!mesh) mesh = std::make_unique<ppgso::Mesh>("monster1.obj");
}

bool Monster::update(Scene &scene, float dt) {
    rotation += rotMomentum * dt;
    position += speed * dt;

    if (position.y < -10)
        return false;
    // Generate modelMatrix from position, rotation and scale
    generateModelMatrix();

    for (auto &obj : scene.objects) {
        if (obj.get() == this) continue;
        auto projectile = dynamic_cast<Projectile*>(obj.get());
        if (!projectile) continue;

        if (distance(position, obj->position) < (obj->scale.y + scale.y) * 1.0f) {
            projectile->destroy();
            return false;
        }
    }

    if (distance(position, scene.nexus->position) < (scene.nexus->scale.y + scale.y) * 0.7f) {
        scene.nexus->destroy();
        return false;
    }

    // Update Shadow
    if(shadow) {
        float MAP_DEPTH = 1.0f;
        float k = MAP_DEPTH / scene.lightDirection.z;
        shadow->position = position;
        shadow->position.y += scene.lightDirection.y * k;
        shadow->position.x += scene.lightDirection.x * k;
        shadow->position.z += MAP_DEPTH;
        shadow->update_shadow(scene, dt);
    }

    return true;
}

bool Monster::update_shadow(Scene &scene, float dt) {

    if (position.y < -10)
        return false;

    // Generate modelMatrix from position, rotation and scale
    generateModelMatrix();

    return true;
}

void Monster::death(Scene &scene, glm::vec3 explosionPosition, glm::vec3 explosionScale) {
    // Generate explosion
    auto explosion = std::make_unique<Explosion>();
    explosion->position = explosionPosition;
    explosion->scale = explosionScale;
    explosion->speed = speed / 2.0f;
    scene.objects.push_back(move(explosion));

}

void Monster::render(Scene &scene) {
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *texture);
    mesh->render();

    if(shadow) {
        shadow->render_shadow(scene);
    }
}

void Monster::render_shadow(Scene &scene) {
    shader->use();

    // Set up light
    shader->setUniform("LightDirection", scene.lightDirection);
    shader->setUniform("camera_position", scene.camera->position);

    // use camera
    shader->setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader->setUniform("ViewMatrix", scene.camera->viewMatrix);

    // render mesh
    shader->setUniform("ModelMatrix", modelMatrix);
    shader->setUniform("Texture", *shadow_texture);
    mesh->render();
}

void Monster::onClick(Scene &scene) {
    std::cout << "Monster clicked!" << std::endl;
    death(scene, position, {10.0f, 10.0f, 10.0f});
}


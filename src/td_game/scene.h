#ifndef _PPGSO_SCENE_H
#define _PPGSO_SCENE_H

#include <memory>
#include <map>
#include <list>

#include "object.h"
#include "level.h"
#include "camera.h"
#include "nexus.h"

/*
 * Scene is an object that will aggregate all scene related data
 * Objects are stored in a list of objects
 * Keyboard and Mouse states are stored in a map and struct
 */
class Scene {
public:
    glm::vec3 wind {0.0f,0.0f , 0.0f};
    glm::vec3 gravity {0.0f, -9.81f, 0.0f};

    /*!
     * Update all objects in the scene
     * @param time
     */
    void update(float dt, float time);

    /*!
     * Render all objects in the scene
     */
    void render();

    /*!
     * Pick objects using a ray
     * @param position - Position in the scene to pick object from
     * @param direction - Direction to pick objects from
     * @return Objects - Vector of pointers to intersected objects
     */
    std::vector<Object *> intersect(const glm::vec3 &position, const glm::vec3 &direction);

    void disable_spawner();
    int spawner_count = 1;
    bool end_game = false;
    bool victory = false;

    // Camera object
    std::unique_ptr<Camera> camera;
    // Level object
    std::unique_ptr<Level> level;

    std::unique_ptr<Nexus> nexus;
    // All objects to be rendered in scene
    std::list<std::unique_ptr<Object> > objects;

    // Spawners and towers
    std::list<std::unique_ptr<Object> > buildings;

    // Keyboard state
    std::map<int, int> keyboard;

    // Lights, in this case using only simple directional diffuse lighting
    glm::vec3 lightDirection{-1.0f, -15.0f, -15.0f};

    // Store cursor state
    struct {
        double x, y;
        bool left, right;
    } cursor;

};

#endif // _PPGSO_SCENE_H

# Tower Defence Game
#### Author: Max Karel
#### Subject: PPGSO @ FIIT STU, 2018/2019

We have created a game where player has to place towers which shoot at moving enemy targets trying to reach the nexus. When colliding with it they remove one health point from the nexus shown as removing one rotating hedron. When all hedrons are removed the player looses. If player manages to destroy all enemies spawned he will complete the level and can move to next one. There are total of 2 levels.

## Solution

In this section I will briefly describe how we achieved some game play mechanics.

### Menu
**Fire Rain with gravity and wind** - here are simple falling objects which add little flair to the menu and simulating the gravity. The code is located in `Menu` and `Rain` classes

**Rotating hedrons** - Hedrons are rotating according to rotation of main nexus. This is achieved by passing ModelMatrix of nexus to hedron and using it in subsequent transformations.
An additional subhedron is added to present 3 level hierarchical transformation

```
modelMatrix = MM; // Rotate whole hedron plane
modelMatrix *=
        // disperse hedrons on circle around point 0.0
        glm::rotate(glm::mat4(1.0f), nexus_rot.z, glm::vec3(0.0f,0.0f, 1.0f))
        * glm::translate(glm::mat4(1.0f),  radius) // with radius
        * glm::scale(glm::mat4(1.0f), scale)
        // Negate translation from nexus
        * glm::translate(glm::mat4(1.0f), -nexus_pos);
modelMatrix *= MM; // Apply same hedron rotation as nexus
```

### Game window
After small delay enemies will began spawning from the alien-like cube ship and start marching towards the nexus. Player than can use left clicks to place defensive towers. The calculation of mouse position in world coordinates is called from main file `td_game.cpp` like this `auto position = scene.camera->to_worldSpace(u, v);` and the code in camera:
```
glm::vec3 Camera::to_worldSpace(double u, double v) {
    glm::vec4 screenPosition{u, v, 0.0f, 1.0f};
    auto invProjection = glm::inverse(projectionMatrix);
    auto invView = glm::inverse(viewMatrix);
    auto planePosition = invView * invProjection * screenPosition;
    planePosition /= planePosition.w;
    auto direction = glm::normalize(planePosition - glm::vec4{position, 1.0f});
    glm::vec3 ray_world = glm::vec3(direction.x, direction.y, direction.z);

    glm::vec3 norm = glm::vec3(0.0,0.0,1.0);
    float d = glm::dot(ray_world, norm);
    if ( d > 1e-6) {
        float t = glm::dot(-position, norm) / d;
        glm::vec3 result = ray_world * t;
        result += position;
        result.z = 0;
        return result;
    }
    return glm::vec3(1.0f);
}
```
As we can see its done by transforming the camera coordinates with view and projection matrix and then simplified raycasting is used for intersection with XY plane located at z=0.


### Camera rotation
User can rotate camera with key `C`. The rotation is done in steps and moves camera between two fixed positions. In order to zoom in/out user can use `Page Up/Page Down` buttons. If animation is turned on camera moves its positional vectors by small increments:
```
if (animateView) {
    position += d_position;
    back += d_back;
    up += d_up;
    glm::vec3 t = position - target_position;
    float distance = glm::length(t);
    if (distance < 0.01) {
        animateView = false;
        position = target_position;
        back = target_back;
        up = target_up;
    }
}
```
## Light
The scene has single source of light moving across X axes to simulate search spotlights. All code is located in the `diffuse_frag.glsl` shader. Where LightDirection is used as direction of light and also position of light when calculating attenuation:
```
// Specular calculation of phong model
vec4 view_vec = normalize(vec4(camera_position, 1.0f) - fragment_position);
vec4 refl_vec = normalize(reflect(-light_dir, normal));

float phong = pow(max(dot(view_vec, refl_vec), 0.0f), 27);
specular = phong * 10;
```
```
// Attenuation and total light calculation
float dist  = distance(light_dir, fragment_position) - 15.0f ;
  if (dist < 0) {
    dist = 0.0;
  }
  float attenuation = 1 / ( 1 + pow(dist, 2)/100.0f);

  float light = clamp(specular, 0 ,1) + clamp( 0.5 * diffuse, 0, 1) ;
  light *= attenuation;
  light += ambient;
  light = clamp(light, 0, 1);
```
## Screen Shots

specular reflection is best visible from side view:
![specular](images/specular.png)

main menu:
![menu](images/menu.png)

damaged nexus with only one HP remaining:
![damaged](images/nexus.png)

tower shooting:
![tower](images/tower.png)

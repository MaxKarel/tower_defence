#version 330
// A texture is expected as program attribute
uniform sampler2D Texture;

// Direction of light
uniform vec3 LightDirection;

// (optional) Transparency
uniform float Transparency;

// (optional) Texture offset
uniform vec2 TextureOffset;

// Camera position
uniform vec3 camera_position;

// Position in worldspace
in vec4 fragment_position;

// The vertex shader will feed this input
in vec2 texCoord;

// Wordspace normal passed from vertex shader
in vec4 normal;

// The final color
out vec4 FragmentColor;

void main() {
  // Compute diffuse lighting
  vec4 light_dir = vec4(LightDirection, 1.0f);
  float ambient = 0.2f;
  float diffuse = max(dot(normal, normalize(light_dir)), 0.0f);
  float specular = 0.0;
  if (true) { //Lamberts law
    vec4 view_vec = normalize(vec4(camera_position, 1.0f) - fragment_position);
    vec4 refl_vec = normalize(reflect(-light_dir, normal));

    float phong = pow(max(dot(view_vec, refl_vec), 0.0f), 27);
    specular = phong * 10;
  }

  float dist  = distance(light_dir, fragment_position) - 15.0f ;
  if (dist < 0) {
    dist = 0.0;
  }
  float attenuation = 1 / ( 1 + pow(dist, 2)/100.0f);

  float light = clamp(specular, 0 ,1) + clamp( 0.5 * diffuse, 0, 1) ;
  light *= attenuation;
  light += ambient;
  light = clamp(light, 0, 1);

  // Lookup the color in Texture on coordinates given by texCoord
  // NOTE: Texture coordinate is inverted vertically for compatibility with OBJ
  FragmentColor = texture(Texture, vec2(texCoord.x, 1.0 - texCoord.y) + TextureOffset) * light;

  FragmentColor.a = Transparency;
}
